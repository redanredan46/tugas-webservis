<?php

$servername = "localhost";
$username = "root";
$pass = "";
$database = "northwind";

try{
    $conn = new PDO("mysql:host=$servername;dbname=$database", $username , $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION);
    
}catch(PDOException $e){
    echo"Connection Failed:".$e->getMessage();
}