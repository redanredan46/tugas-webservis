<?php

$data = file_get_contents("https://data.covid19.go.id/public/api/skor.json");
$covid = json_decode($data , true);

?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Latihan 2</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  </head>
  <body>

  <div class="container ">
    <div class="alert alert-primary mt-3" role="alert"> <h3 class="mt-3 text-center">DATA RESIKO COVID-19 SELURUH PROVINSI DI INDONESIA</h3> </div>
       
        <h5 class="mt-3">Data Pada : <?= $covid['tanggal']; ?></h5>
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">No</th>
                <th scope="col">Provinsi</th>
                <th scope="col">Kode_Provinsi</th>
                <th scope="col">Kota</th>
                <th scope="col">Kode_Kota</th>
                <th scope="col">Hasil</th>
                </tr>
            </thead>

            <?php 
            $no = 1;
            foreach( $covid['data'] as $row) : ?>
            <tbody>
                <tr>
                <th scope="row"><?= $no++;?></th>
                <td><?= $row["prov"]?></td>
                <td><?= $row["kode_prov"]?></td>
                <td><?= $row["kota"]?></td>
                <td><?= $row["kode_kota"]?></td>
                <td><?= $row["hasil"]?></td>
                </tr>
            </tbody>
            <?php endforeach; ?>
        </table>
        
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
</html>